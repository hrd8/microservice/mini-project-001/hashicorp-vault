#!/bin/sh
a=0
mkdir -p "/home/token"
mkdir -p "/home/config-json"
if [[ "$(vault status | grep Sealed | awk '{print $2}')" == "true" ]]; then
    if [[ "$(vault operator init --status)" = "Vault is not initialized" ]]; then
        vault operator init >/home/token/token.txt
    fi
    cat /home/token/token.txt | grep "Unseal Key" | awk '{print $4}' | while read -r line; do
        if [[ $a -lt 3 ]]; then
            vault operator unseal $line
        fi
        a=$(($a + 1))
    done
fi
if [[ "$(vault status | grep Sealed | awk '{print $2}')" == "false" ]]; then
    vault login "$(cat /home/token/token.txt | grep Token | awk '{print $4}')"
    if [[ $1 == "-i" || $1 == "--init" ]]; then
        vault secrets enable -version=2 kv
        vault kv put kv/registry,dev @/home/config-json/registry-dev.json
        vault kv put kv/gateway,dev @/home/config-json/gateway-dev.json
        vault kv put kv/auth,dev @/home/config-json/auth-dev.json
    fi
fi
