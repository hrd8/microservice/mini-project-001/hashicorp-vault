# Run Instruction

## To start the server please execute

    docker-compose up -d

## Please execute the init script below

    docker exec mn-vault-one sh /home/init-script.sh -i

## Or

    docker exec mn-vault-one sh /home/init-script.sh --init

## ignore -i and --init if run the second time

## after done please copy the root token and replace the token in config-server

    spring.config.server.vault.token: ${Root token from /vault/token/token.txt}
